from django.contrib import admin
from inca_backend.models import Log, WhatIsMoreEntry
# Register your models here.
admin.site.register(Log)
admin.site.register(WhatIsMoreEntry)

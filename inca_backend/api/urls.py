from tkinter import Entry
from django.urls import path,include
from rest_framework.routers import DefaultRouter
# from inca_backend.api.views import log_details, log_list
# from inca_backend.api.views import EntryListAV,EntryDetailAV,LogListAV
from inca_backend.api.views import  LogVS, WhatIsMoreEntryList, WhatIsMoreEntryVS

router = DefaultRouter()
router.register('entries',WhatIsMoreEntryVS,basename='entries')
router.register('',LogVS,basename='logs')

urlpatterns = [
    path('',include(router.urls)),
    path('<int:pk>/entries/',WhatIsMoreEntryList.as_view(), name='entry-list'),
    #path('create/',LogCreate.as_view(), name='log-create')
    #path('entries/', EntryListAV.as_view(), name='entry-list'),
    #path('entries/<int:pk>', EntryDetailAV.as_view(), name='entry-details'),
    #path('list/', LogListAV.as_view(), name='log-list'),
]

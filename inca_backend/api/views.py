from rest_framework.response import Response
# from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework import status
from rest_framework import generics
from rest_framework import viewsets
from inca_backend.api.serializers import LogSerializer, WhatIsMoreEntrySerializer
from inca_backend.models import Log, WhatIsMoreEntry
from inca_backend.api.permissions import AdminOrReadonly, UserOrReadOnly
from rest_framework.permissions import IsAuthenticated


class WhatIsMoreEntryVS(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = WhatIsMoreEntry.objects.all()
    serializer_class = WhatIsMoreEntrySerializer


class LogVS(viewsets.ModelViewSet):
    queryset = Log.objects.all()
    serializer_class = LogSerializer
    permission_classes = [IsAuthenticated]

    def create(self, request):
        serializer = LogSerializer(data=request.data)
        trainer = self.request.user
        print('TRAINER', trainer)
        serializer.is_valid(raise_exception=True)
        serializer.save(trainer=trainer)
        return Response(serializer.data)


class WhatIsMoreEntryList(generics.ListAPIView):
    serializer_class = WhatIsMoreEntrySerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        pk = self.kwargs['pk']
        return WhatIsMoreEntry.objects.filter(log=pk)


# class EntryListAV(APIView):
#     def get(self,request):
#         entry = Entry.objects.all()
#         serializer = EntrySerializer(entry, many=True)
#         return Response(serializer.data)

#     def post(self,request):
#         serializer = EntrySerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         else:
#             return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

# class EntryDetailAV(APIView):
#     def get(self,request,pk):
#         try:
#             entry = Entry.objects.get(pk=pk)
#         except Entry.DoesNotExist:
#             return Response({'error':'Entry not found'},status=status.HTTP_400_BAD_REQUEST)
#         serializer = EntrySerializer(entry)
#         return Response(serializer.data)

#     def put(self,request,pk):

#         entry = Entry.objects.get(pk=pk)
#         serializer = EntrySerializer(entry,data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         else:
#             return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#     def delete(self,request,pk):

#        log = Entry.objects.get(pk=pk)
#        log.delete()
#        return Response(status=status.HTTP_204_NO_CONTENT)


# class LogListAV(APIView):
#     def get(self,request):
#         log = Log.objects.all()
#         serializer = LogSerializer(log, many=True)
#         return Response(serializer.data)

#     def post(self,request):
#         serializer = LogSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         else:
#             return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

# @api_view(['GET','POST'])
# def log_list(request):
#     if request.method =='GET':
#         logs = Log.objects.all()
#         serializer = LogSerializer(logs,many=True)
#         return Response(serializer.data)

#     if request.method == 'POST':
#         serializer = LogSerializer(data=request.data)

#     if serializer.is_valid():
#         serializer.save()
#         return Response(serializer.data)

#     else:
#         return Response(serializer.error,status=status.HTTP_400_BAD_REQUEST)

# @api_view(['GET','PUT','DELETE'])
# def log_details(request,pk):

#     if request.method == 'GET':
#         try:
#             log = Log.objects.get(pk=pk)
#         except Log.DoesNotExist:
#             return Response({'error':'Log not foud'},status=status.HTTP_400_BAD_REQUEST)
#         serializer = LogSerializer(log)

#         return Response(serializer.data)
#     if request.method == 'PUT':
#         log = Log.objects.get(pk=pk)
#         serializer = LogSerializer(log,data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         else:
#             return Response(serializer.data)
#     if request.method == 'DELETE':
#         log = Log.objects.get(pk=pk)
#         log.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)

from rest_framework import serializers
from inca_backend.models import Log,WhatIsMoreEntry



class WhatIsMoreEntrySerializer(serializers.ModelSerializer):

    class Meta:
        model = WhatIsMoreEntry
        fields = "__all__"

class LogSerializer(serializers.ModelSerializer):
    log_entries = WhatIsMoreEntrySerializer(many=True,read_only=True)
    trainer = serializers.StringRelatedField(read_only=True)
    class Meta:
        model = Log
        fields = "__all__"

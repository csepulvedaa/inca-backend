from django.apps import AppConfig


class IncaBackendConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'inca_backend'

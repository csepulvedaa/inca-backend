from django.db import models
from django.contrib.auth.models import User

class Log(models.Model):
    date = models.DateField()
    trainer = models.ForeignKey(User,on_delete=models.CASCADE)
    subject = models.TextField(max_length=255)
    laboratory = models.TextField(max_length=255)
    app_name = models.TextField(max_length=255)
    def __str__(self) -> str:
        return self.subject + ' -  ' + str(self.date)

class WhatIsMoreEntry(models.Model):
    hour = models.TimeField()
    test_name = models.CharField(max_length=255)
    c0 = models.TextField()
    c1 = models.TextField()
    c2 = models.TextField()
    c3 = models.TextField()
    c4 = models.TextField()
    log =  models.ForeignKey(Log,on_delete=models.CASCADE, related_name="log_entries")
    result = models.BooleanField()
    response_time = models.TextField(max_length=255)
    other = models.TextField(max_length=255)